GOPKGSRC ?= $(wildcard *.go)
GOTESTSRC ?= $(wildcard *_test.go)
GOSRC ?= $(filter-out $(GOTESTSRC),$(GOPKGSRC))
GOTESTDATA ?= $(shell test ! -d testdata || find testdata)

GOMAINSRC ?= $(wildcard cmd/*/main.go)
GOBIN ?= $(patsubst cmd/%/main.go,%,$(GOMAINSRC))

GOCOVERAGE ?= go.coverage
GOBENCHMARK ?= go.benchmark
GOTESTFLAGS ?= -race -cover

%: cmd/%/main.go $(GOPKGSRC)
	go build -o $@ $(GOBUILDFLAGS) $<

$(GOCOVERAGE).out: $(GOPKGSRC) $(GOTESTSRC) $(GOTESTDATA) go.mod
	$(MAKE) test GOTESTFLAGS="$(GOTESTFLAGS) -coverprofile=$@"

$(GOCOVERAGE).html: $(GOCOVERAGE).out
	go tool cover -html $< -o $@

bench:: $(GOBENCHMARK)

$(GOBENCHMARK): GOBENCHFLAGS ?= -benchmem
$(GOBENCHMARK): $(GOPKGSRC) $(GOTESTSRC) $(GOTESTDATA) go.mod
	go test -bench . $(GOBENCHFLAGS) | tee $@

lint::
	golangci-lint run -E gofmt,golint

test::
	go test $(GOTESTFLAGS) ./...

clean::
	$(RM) $(GOCOVERAGE).html $(GOCOVERAGE).out $(GOBENCHMARK) $(GOBIN)

.PHONY: clean lint test $(GOCOVERAGE)
.DELETE_ON_ERROR:
