package lexer

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestLexer(t *testing.T) {
	for _, c := range []struct {
		input    string
		expected []string
	}{
		{"", []string{}},
		{"\t", []string{}},
		{" ab ", []string{"ab"}},
		{"aaabxy", []string{"aaab", "x", "y"}},
	} {
		t.Run(c.input, func(t *testing.T) {
			lexer := NewLexer(abxy, strings.NewReader(c.input))
			actual := make([]string, 0, len(c.expected))
			for lexer.Lex() {
				actual = append(actual, lexer.Text())
			}
			assert.NoError(t, lexer.Err())
			assert.Equal(t, c.expected, actual)
		})
	}
}

func TestLexerBufferPanics(t *testing.T) {
	lexer := NewLexer(abxy, strings.NewReader(`ab`))
	lexer.Buffer(make([]byte, 3))
	assert.True(t, lexer.Lex())
	assert.Panics(t, func() { lexer.Buffer(make([]byte, 4096)) },
		"setting a buffer after lexing should fail")
}

func TestLexerUnmatchedError(t *testing.T) {
	lexer := NewLexer(abxy, strings.NewReader(`ab c x y`))
	var actual []string
	for lexer.Lex() {
		actual = append(actual, lexer.Text())
	}
	assert.Equal(t, []string{"ab"}, actual)
	assert.EqualError(t, lexer.Err(), `unmatched data: ‘c x y’`)
}

func TestLexerFull(t *testing.T) {
	lexer := NewLexer(abxy, strings.NewReader(`aaaaaaaab`))
	lexer.Buffer(make([]byte, 8))
	assert.False(t, lexer.Lex())
	assert.Equal(t, lexer.Err(), ErrFull)
}

func TestLexerUnmatchedTruncated(t *testing.T) {
	lexer := NewLexer(abxy, strings.NewReader(`ab c `+strings.Repeat(`x`, 1024)))
	var actual []string
	for lexer.Lex() {
		actual = append(actual, lexer.Text())
	}
	assert.Equal(t, []string{"ab"}, actual)
	assert.EqualError(t, lexer.Err(),
		`unmatched data: ‘c xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx’ + 976 bytes`)
}

func TestLexerTypes(t *testing.T) {
	lexer := NewLexer(abxy, strings.NewReader(`aab x y`))
	var actual []interface{}
	for lexer.Lex() {
		actual = append(actual, lexer.Token().Type)
	}
	assert.Equal(t, []interface{}{"ab", 'x', "y"}, actual)
}

func TestLexerReject(t *testing.T) {
	lexer := NewLexer(abxy, strings.NewReader(`ab z x y`))
	var actual []string
	for lexer.Lex() {
		actual = append(actual, lexer.Text())
	}
	assert.Equal(t, []string{"ab"}, actual)
	assert.EqualError(t, lexer.Err(), `rejected token: ‘z’`)
}

func BenchmarkLexer(b *testing.B) {
	bs, err := ioutil.ReadFile("testdata/abxy")
	require.NoError(b, err)
	buf := make([]byte, 16)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		rdr := bytes.NewReader(bs)
		lexer := NewLexer(abxy, rdr)
		lexer.Buffer(buf)
		tokens := 0
		for lexer.Lex() {
			tokens++
		}
		assert.Equal(b, 20000, tokens)
		assert.NoError(b, lexer.Err())
	}
}

// A simple lexicon which tokenizes arithmetic expressions with numbers
// and single-character variables. (All types are runes for convenience
// when printing, but this isn’t a requirement for lexicons.)
func Example_arithmetic() {
	arithmetic := NewLexicon([]Definition{
		{Type: '('},
		{Type: ')'},
		{Type: '+'},
		{Type: '-'},
		{Type: '×'},
		{Type: '÷'},
		{Type: 'i', Pattern: "[1-9][0-9]*", Eval: Int},
		{Type: 'v', Pattern: "[a-z]"},
		DiscardWhitespace,
	})

	expr := "y + 15 × (x - 23)"
	lexer := NewLexer(arithmetic, strings.NewReader(expr))
	for lexer.Lex() {
		token := lexer.Token()
		fmt.Printf("%c :  %s\n", token.Type, token.Text())
	}
	if lexer.Err() != nil {
		panic(lexer.Err())
	}
	// Output:
	// v :  y
	// + :  +
	// i :  15
	// × :  ×
	// ( :  (
	// v :  x
	// - :  -
	// i :  23
	// ) :  )
}

func ExampleLexer_unmatched_text() {
	numbers := NewLexicon([]Definition{
		{Type: 'i', Pattern: "[1-9][0-9]*"},
		{Type: Discard, Pattern: ","},
	})

	input := "1,2,3,not a number"
	lexer := NewLexer(numbers, strings.NewReader(input))
	for lexer.Lex() {
		token := lexer.Token()
		fmt.Printf("%c :  %s\n", token.Type, token.Text())
	}
	fmt.Printf("Halted with: %s", lexer.Err())
	// Output:
	// i :  1
	// i :  2
	// i :  3
	// Halted with: unmatched data: ‘not a number’
}

func ExampleEvaluator() {
	numbers := NewLexicon([]Definition{
		{Type: 'i', Pattern: "[1-9][0-9]*", Eval: Int},
		{Type: Discard, Pattern: ","},
	})

	input := "1,2,3,12345678901234567890"
	lexer := NewLexer(numbers, strings.NewReader(input))
	for lexer.Lex() {
		token := lexer.Token()
		fmt.Printf("%c :  %d\n", token.Type, token.Value.(int))
	}
	fmt.Printf("Halted with: %s", lexer.Err())
	// Output:
	// i :  1
	// i :  2
	// i :  3
	// Halted with: strconv.Atoi: parsing "12345678901234567890": value out of range
}
