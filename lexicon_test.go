package lexer

import (
	"bufio"
	"bytes"
	"io/ioutil"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestLexiconInvalid(t *testing.T) {
	assert.Panics(t, func() {
		NewLexicon(nil)
	}, "a lexicon with no definitions should panic")

	assert.Panics(t, func() {
		NewLexicon([]Definition{{Pattern: "a*"}})
	}, "a pattern matching an empty string should panic")

	assert.Panics(t, func() {
		NewLexicon([]Definition{{Pattern: "*"}})
	}, "an invalid pattern should panic")

	assert.Panics(t, func() {
		NewLexicon([]Definition{{Type: 1}})
	}, "an empty pattern without a string/rune type should panic")
}

var abxy = NewLexicon([]Definition{
	{Type: "ab", Pattern: "a+b"}, // explicit pattern
	{Type: 'x'},                  // pattern assumed from rune type
	{Type: "y"},                  // pattern assumed from string type
	{Type: 'z', Eval: Reject},
	DiscardWhitespace,
})

func TestLexicon(t *testing.T) {
	for _, c := range []struct {
		input    string
		expected []string
	}{
		{"", []string{}},
		{"\t", []string{}},
		{" ab ", []string{"ab"}},
		{"aaabxy", []string{"aaab", "x", "y"}},
	} {
		t.Run(c.input, func(t *testing.T) {
			scanner := bufio.NewScanner(strings.NewReader(c.input))
			scanner.Split(abxy.Split)
			actual := make([]string, 0, len(c.expected))
			for scanner.Scan() {
				actual = append(actual, scanner.Text())
			}
			assert.NoError(t, scanner.Err())
			assert.Equal(t, c.expected, actual)
		})
	}
}

func TestUnmatchedError(t *testing.T) {
	scanner := bufio.NewScanner(strings.NewReader(`ab c x y`))
	scanner.Split(abxy.Split)
	var actual []string
	for scanner.Scan() {
		actual = append(actual, scanner.Text())
	}
	assert.Equal(t, []string{"ab"}, actual)
	assert.EqualError(t, scanner.Err(), `unmatched data: ‘c x y’`)
}

func TestUnmatchedTruncated(t *testing.T) {
	scanner := bufio.NewScanner(strings.NewReader(`ab c ` + strings.Repeat(`x`, 1024)))
	scanner.Split(abxy.Split)
	var actual []string
	for scanner.Scan() {
		actual = append(actual, scanner.Text())
	}
	assert.Equal(t, []string{"ab"}, actual)
	assert.EqualError(t, scanner.Err(),
		`unmatched data: ‘c xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx’ + 976 bytes`)
}

func TestReject(t *testing.T) {
	scanner := bufio.NewScanner(strings.NewReader(`ab z x y`))
	scanner.Split(abxy.Split)
	var actual []string
	for scanner.Scan() {
		actual = append(actual, scanner.Text())
	}
	assert.Equal(t, []string{"ab"}, actual)
	assert.EqualError(t, scanner.Err(), `rejected token: ‘z’`)
}

func TestBufferAlign(t *testing.T) {
	scanner := bufio.NewScanner(strings.NewReader(`x y aaaaaaab x`))
	scanner.Split(abxy.Split)
	scanner.Buffer(make([]byte, 9), 8)
	var actual []string
	for scanner.Scan() {
		actual = append(actual, scanner.Text())
	}
	assert.Equal(t, []string{"x", "y", "aaaaaaab", "x"}, actual)
	assert.NoError(t, scanner.Err())
}

func TestFuzzCorpus(t *testing.T) {
	corpus, err := filepath.Glob("testdata/corpus/*")
	require.NoError(t, err)
	for _, filename := range corpus {
		t.Run(filepath.Base(filename), func(t *testing.T) {
			bs, err := ioutil.ReadFile(filename)
			require.NoError(t, err)
			abxy.Fuzz(bs)
		})
	}
}

func TestLexiconSplitBufferAlignment(t *testing.T) {
	bs, err := ioutil.ReadFile("testdata/abxy")
	require.NoError(t, err)
	tokens := make([][]string, 11)
	for i := range tokens {
		scanner := bufio.NewScanner(bytes.NewReader(bs))
		scanner.Split(abxy.Split)
		scanner.Buffer(make([]byte, 16+i), 16)
		for scanner.Scan() {
			tokens[i] = append(tokens[i], scanner.Text())
		}
		assert.NoError(t, scanner.Err(),
			"scan error at buffer size %d", 16+i)
	}
	for i := 1; i < len(tokens); i++ {
		assert.Equal(t, 20000, len(tokens[i]),
			"mis-scan at buffer size %d", 16+i)
		assert.Equal(t, tokens[i-1], tokens[i],
			"scan mismatch between buffer sizes %d and %d",
			16+i, 16+i+1)
	}
}

func BenchmarkLexiconSplit(b *testing.B) {
	bs, err := ioutil.ReadFile("testdata/abxy")
	require.NoError(b, err)
	buf := make([]byte, 16)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		rdr := bytes.NewReader(bs)
		scanner := bufio.NewScanner(rdr)
		scanner.Split(abxy.Split)
		scanner.Buffer(buf, len(buf))
		tokens := 0
		for scanner.Scan() {
			tokens++
		}
		assert.Equal(b, 20000, tokens)
		assert.NoError(b, scanner.Err())
	}
}
