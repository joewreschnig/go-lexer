package lexer

import (
	"bytes"
	"fmt"
	"regexp"
)

// A Lexicon defines rules for lexical analysis of a byte stream using a
// sequence of regular expression patterns. The patterns are matched
// against the start of the input data to produce a []byte lexeme. When
// used with a Lexer this is then annotated with a type and optionally
// evaluated to make a Token.
//
// The lexicon’s precedence rules are controlled by the order of
// entries; matches from earlier entries are preferred.
//
// The patterns in a lexicon should have fixed terminators; otherwise
// the result of lexing will depend on how much data was available in
// the lexer’s buffer past the shortest possible match, which will vary
// depending on the position of the beginning of the match in the input.
// You can ensure this using the “prefer fewer” (a.k.a. “shy”)
// repetition operators like ‘*?’ and ‘+?’, or explicit delimiters e.g.
// ‘`[^`]*`’. The lexer will also ensure there is at least one character
// present after the match (or no characters left in the input) before
// lexing it, so a pattern like “//.*” to match line-terminated comments
// works.
//
// A Lexicon is stateless and can be safely used by multiple Lexers.
type Lexicon struct {
	entries  []Definition
	matchers []*regexp.Regexp
}

// NewLexicon compiles the provided entries into a lexicon. If any
// pattern doesn't meet the requirements this will panic; lexer rules
// are expected to be hard-coded.
func NewLexicon(entries []Definition) *Lexicon {
	if len(entries) == 0 {
		panic("lexers must have at least one pattern")
	}
	matchers := make([]*regexp.Regexp, len(entries))
	for i, dfn := range entries {
		pattern := dfn.Pattern
		if pattern == "" {
			switch t := dfn.Type.(type) {
			case rune:
				pattern = regexp.QuoteMeta(string(t))
			case string:
				pattern = regexp.QuoteMeta(t)
			default:
				panic("lexicon entries must have a pattern or string/rune type")
			}
		}

		matchers[i] = regexp.MustCompile("^" + pattern)
		if matchers[i].Match(nil) {
			panic("lexer regexes must not match an empty string")
		}

	}
	return &Lexicon{
		entries:  entries,
		matchers: matchers,
	}
}

// Definition defines a lexicon entry for a Lexer.
type Definition struct {
	// If the type is Discard the matching lexeme will be skipped
	// (see e.g. DiscardWhitespace). Otherwise this can be used to find
	Type interface{}

	// A regular expression pattern to match a lexeme. It must not
	// match an empty sequence; if it does NewLexicon will panic.
	//
	// If empty and the Type is a string or rune, the definition
	// matches the literal of the type. (Useful for concise
	// definition of operations and keywords.)
	Pattern string

	// If non-nil, the evaluator’s result is stored in a Token's
	// Value instead of the matched lexeme. If an error is returned
	// scanning stops.
	Eval Evaluator
}

type discard struct{}

// Discard may be provided as a definition’s type to throw out the
// matched lexeme and continue processing.
var Discard discard

// Split implements a bufio.Scanner-compatible SplitFunc based on the
// lexicon.
//
// Evaluators are still run to check for errors but their return value
// is ignored; bufio.Scanner only exposes each lexeme. If there is data
// remaining in the input which cannot be matched, an UnmatchedError (up
// to the maximum token length) is returned.
//
// As lexicon entries only match when there is at least one unmatched
// character in the input, the maximum token size of a bufio.Scanner is
// effectively reduced by one.
func (s *Lexicon) Split(data []byte, atEOF bool) (advance int, lexeme []byte, err error) {
	return s.split(data, nil, atEOF)
}

func (s *Lexicon) split(data []byte, t *Token, atEOF bool) (advance int, lexeme []byte, err error) {
	var i int
retry: // see Discard handling
	for i = range s.matchers {
		if lexeme = s.matchers[i].Find(data); lexeme != nil {
			break
		}
	}
	if lexeme == nil {
		if atEOF && len(data) > 0 {
			err = UnmatchedError(data)
		}
		return
	}
	if len(lexeme) == len(data) && !atEOF {
		// we need at least one character beyond the match
		return advance, nil, nil
	}
	advance += len(lexeme)
	dfn := &s.entries[i]
	if t != nil {
		*t = Token{
			Definition: dfn,
			Lexeme:     lexeme,
		}
	}
	switch {
	case dfn.Type == Discard:
		data = data[len(lexeme):]
		goto retry
	case dfn.Eval != nil:
		if t == nil {
			_, err = dfn.Eval(lexeme)
		} else {
			t.Value, err = dfn.Eval(lexeme)
		}
	}
	return
}

// An UnmatchedError is returned when no lexemes at the beginning of the
// text can be matched.
//
// If a Scanner or Lexer’s error is an UnmatchedError its contents are
// only valid until the next call to Scan() or Lex() (which should never
// happen after an error occurs) or its buffer is otherwise reused.
type UnmatchedError []byte

func (err UnmatchedError) Error() string {
	if len(err) > 60 {
		return fmt.Sprintf("unmatched data: ‘%s’ + %d bytes",
			[]byte(err[:50]), len(err)-50)
	}
	return fmt.Sprintf("unmatched data: ‘%s’", string(err))
}

// DiscardWhitespace is a useful final entry in a lexicon in order to
// ignore any otherwise unmatched whitespace before, between, or after
// other meaningful tokens.
var DiscardWhitespace = Definition{Type: Discard, Pattern: `[\pZ[:space:]]+`}

var _ = NewLexicon([]Definition{
	DiscardWhitespace,
})

// Fuzz helps you fuzz your lexers using go-fuzz; the data controls the
// buffer size and input bytes, and returns -1 if the data was too short
// to be useful, 0 if the data was lexed unsuccessfully, and 1 if it was
// lexed successfully.
func (s *Lexicon) Fuzz(data []byte) int {
	// read off the first two bytes of the fuzz data as the buffer
	// size, the rest as input data.
	if len(data) < 2 {
		return -1
	}
	lexer := NewLexer(s, bytes.NewReader(data[2:]))
	lexer.Buffer(make([]byte, 256*int(data[0])+int(data[1])))
	for lexer.Lex() {
		lexer.Text()
	}
	if lexer.Err() == nil {
		return 1
	}
	return 0
}
