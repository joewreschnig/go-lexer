package lexer

import (
	"fmt"
	"strconv"
)

// An Evaluator converts a lexeme from its raw bytes into a more
// appropriate value e.g. string unescaping or integer parsing. Lexicon
// entries without evaluators use the lexeme as the value.
type Evaluator = func([]byte) (interface{}, error)

// Reject is an Eval function which halts scanning and returns a
// RejectedError, regardless of the lexeme.
func Reject(lexeme []byte) (interface{}, error) {
	return nil, RejectedError(lexeme)
}

// A RejectedError is returned when a lexeme is matched by a definition
// using the Reject Evaluator.
type RejectedError string

func (err RejectedError) Error() string {
	return fmt.Sprintf("rejected token: ‘%s’", string(err))
}

// Int parses the lexeme using strconv.Atoi, producing an int.
func Int(lexeme []byte) (interface{}, error) {
	return strconv.Atoi(string(lexeme))
}

var (
	_ Evaluator = Reject
)
