package lexer

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTokenText(t *testing.T) {
	for expected, tok := range map[string]Token{
		"12345": {Value: int32(12345)},
		"1.25":  {Value: 1.25},
		"abc":   {Value: "abc"},
		"xyz":   {Value: []byte("xyz")},
	} {
		t.Run(expected, func(t *testing.T) {
			assert.Equal(t, expected, tok.Text())
		})
	}
}
