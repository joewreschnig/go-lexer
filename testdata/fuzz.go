package lexerfuzz

import (
	"gitlab.com/joewreschnig/go-lexer"
)

func Fuzz(data []byte) int {
	return lexer.NewLexicon([]lexer.Definition{
		{Type: "ab", Pattern: "a+b"},
		{Type: 'x'},
		{Type: "y"},
		{Type: 'z', Eval: lexer.Reject},
		lexer.DiscardWhitespace,
	}).Fuzz(data)
}
