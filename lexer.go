// Package lexer provides tools for regexp-based lexical analysis of a byte stream.
//
// A Lexer processes an input byte stream into a series of discrete
// tokens according to a table of regular expressions called its
// Lexicon.
//
// The design of Lexer is based on bufio.Scanner with the Lexicon
// replacing the role of its SplitFunc. Lexicons are usually easier to
// specify than writing an equivalent SplitFunc implementation, and allow
// further annotation of lexemes with a type and minimal analysis such as
// integer parsing.
package lexer

import (
	"bufio"
	"errors"
	"io"
)

// A Lexer processes an input byte stream into a series of discrete
// tokens, via the entries in a Lexicon.
//
// The design of Lexer is based on bufio.Scanner, with the Lexicon
// replacing the role of its SplitFunc. Lexicons are usually easier to
// specify than an equivalent SplitFunc implementation and provide a
// degree of lexical analysis as well as scanning.
type Lexer struct {
	lexicon    *Lexicon
	r          io.Reader
	err        error
	buf        []byte
	scanned    bool
	current    Token
	start, end int
}

// NewLexer creates a new lexer, analyzing the reader using the lexicon.
func NewLexer(lexicon *Lexicon, r io.Reader) *Lexer {
	return &Lexer{
		lexicon: lexicon,
		r:       r,
	}
}

// Buffer controls the buffer size when reading. The maximum token size
// is one fewer than the buffer size.
//
// Unlike bufio.Scanner this may not be the only allocation required by
// a Lexer. Complex lexicon patterns may trigger allocations, as will
// most evaluators.
func (l *Lexer) Buffer(buf []byte) {
	// TODO: Scanner specifies token size and buffer size
	// separately; why? Presumably so when it encounters only small
	// tokens it uses a smaller buffer; but this seems useless as a
	// memory guarantee (it could get bigger anyway) and pays a high
	// price if it has to grow the buffer.
	if l.scanned {
		panic("Buffer called after Lex")
	}
	l.buf = buf
}

// Token returns the most recent token extracted by a call to Lex. The
// Token’s Lexeme field remains valid only until the next call to Lex.
func (l *Lexer) Token() Token {
	return l.current
}

// Err returns the first non-EOF error that was encountered by the
// Lexer.
func (l *Lexer) Err() error {
	if l.err == io.EOF {
		return nil
	}
	return l.err
}

// ErrFull is triggered when the lexer cannot find a match in the
// lexicon and the buffer is already its maximum size. This could be
// mean the lexer needs a bigger buffer to find a complete token; or it
// could mean the input is invalid.
var ErrFull = errors.New("lexer: buffer full and no match found")

// Lex advances the Lexer to the next token, which will then be
// available through the Token or Text method. It returns false when the
// scan stops, either by reaching the end of the input or an error.
//
// After Lex returns false, the Err method will return the first
// non-EOF error that occurred during scanning; or nil if EOF was
// reached and all input was lexed successfully.
func (l *Lexer) Lex() bool {
	if l.buf == nil {
		l.buf = make([]byte, bufio.MaxScanTokenSize)
	}
	l.scanned = true

retry:
	switch {
	case l.err == io.EOF && l.start == l.end: // all input lexed
		return false
	case l.err != nil && l.err != io.EOF: // error, halted
		return false
	case l.start >= l.end && !l.refill(): // no more data
		return false
	}

	n, lexeme, err := l.lexicon.split(
		l.buf[l.start:l.end], &l.current, l.err == io.EOF)
	l.start += n
	switch {
	case err != nil: // error, halted
		l.err = err
		return false
	case lexeme != nil: // found something
		return true
	default: // ask for more data
		l.refill()
		goto retry
	}
}

// Refill the lexer’s internal buffer; move it to the beginning of the
// array and then read as much as possible. Returns true if it could
// read anything new.
func (l *Lexer) refill() bool {
	if l.start == 0 && l.end == len(l.buf) {
		l.err = ErrFull
	}
	if l.err != nil {
		return false
	}
	if l.start > 0 {
		copy(l.buf, l.buf[l.start:l.end])
		l.end = l.end - l.start
		l.start = 0
	}
	n := 0
	for n == 0 && l.err == nil {
		n, l.err = l.r.Read(l.buf[l.end:])
		l.end += n
	}
	return n > 0
}

// Text returns the string value of the most recent token extracted by a
// call to Lex.
func (l *Lexer) Text() string {
	return l.current.Text()
}
