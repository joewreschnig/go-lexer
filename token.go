package lexer

import (
	"fmt"
)

// A Token is the result of scanning a lexeme, annotating it with its
// type, and then evaluating it.
type Token struct {
	// The Definition which produced this token.
	*Definition

	// The matched lexeme; only valid until the next call to Lex.
	Lexeme []byte

	// The result of evaluation.
	Value interface{}
}

// Text returns a string representation of the Token’s value. If the
// token has no Value, this string is the lexeme; otherwise it is the
// value formatted as a string.
func (t Token) Text() string {
	switch v := t.Value.(type) {
	case nil:
		return string(t.Lexeme)
	default:
		return fmt.Sprint(t.Value)
	// but this can be done more efficiently in many cases.
	case string:
		return v
	case []byte:
		return string(v)
	}
}
