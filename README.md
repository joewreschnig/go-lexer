# `lexer`

A `Lexer` processes an input byte stream into a series of discrete
tokens according to a table of regular expressions called its `Lexicon`.

The design of `Lexer` is based on `bufio.Scanner` with the Lexicon
replacing the role of its `SplitFunc`. Lexicons are usually easier to
specify than writing an equivalent `SplitFunc` implementation, and allow
further annotation of lexemes with a type and minimal analysis such as
integer parsing.

`Lexicon`s may also be used as a `bufio.Scanner` `SplitFunc`, in which
case they will perform tokenization but no further lexical analysis.

## Licensing

Copyright 2020 Joe Wreschnig

This program is free software: you can redistribute it and/or modify it
under the terms of the [GNU General Public License](COPYING) as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
